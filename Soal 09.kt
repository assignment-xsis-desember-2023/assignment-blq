fun main() {
    // Input
    print("N = ")
    val n = readln().toInt()

    // Output
    for (i in 1..n) {
        print(n*i)
        if (i != n) print(" ")
    }
}