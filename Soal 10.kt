fun main() {
    // Input
    print("Input a name: ")
    val name = readln()

    // Output
    val words = name.split(" ")
    for ((index, word) in words.withIndex()) {
        print(word.first())
        if (word.length > 1) {
            for (i in 1..3) print("*")
            print(word.last())
        }
        if (index != words.lastIndex) {
            print(" ")
        }
    }
}