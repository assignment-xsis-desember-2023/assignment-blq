fun main() {
    // Input
    print("Input a word: ")
    val input = readln()
    val word = input.replace(" ", "").lowercase()

    // Logic
    val isPalindrome = determineWordIsPalindrome(word)

    // Output
    when (isPalindrome) {
        true -> print("$input is a palindrome")
        false -> print("$input is not a palindrome")
    }
}

// Function requested by assignment
fun determineWordIsPalindrome(word: String): Boolean {
    for (i in 0..word.length/2) {
        if (word[i] != word[word.lastIndex - i]) {
            return false
        }
    }
    return true
}