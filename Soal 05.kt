fun main() {
    // Input
    print("Input an integer: ")
    val n = readln().toInt()

    // Output
    printFirstFibonacciNumbers(n)
}

// Function requested by the assignment
fun printFirstFibonacciNumbers(n: Int, start1: Int = 0, start2: Int = 1) {
    var fib1 = start1
    var fib2 = start2
    var temp: Int
    for (i in 1..n) {
        print(fib1)
        if (i != n) {
            print(" ")
            temp = fib1
            fib1 = fib2
            fib2 = fib1 + temp
        }
    }
}