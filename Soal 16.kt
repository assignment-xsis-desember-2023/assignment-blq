fun main() {
    // Constants
    val taxAndServiceMultiplier = 1.15

    // Input
    //  42K (mengandung ikan), 50K, 30K, 70K, 30K
    print("Berapa banyak makanan yang dipesan: ")
    val menuCount = readln().trim().toInt()
    val menu = mutableListOf<Pair<Int, Boolean>>()
    for (i in 1..menuCount) {
        print("Harga makanan ke-$i: ")
        val price = readln().toInt()
        print("Apakah makanan ke-$i mengandung ikan (Y/N)? ")
        val containsFish = (readln().trim().uppercase() == "Y")
        menu.add(price to containsFish)
    }

    // Logic
    val priceShares = Array(4){0.0}
    for (food in menu) {
        if (food.second) { // Jika mengandung ikan
            val priceShare = food.first * taxAndServiceMultiplier / 3.0
            for (i in priceShares.indices) {
                if (i != 3) priceShares[i] += priceShare
            }
        } else { // Jika tidak mengandung ikan
            val priceShare = food.first * taxAndServiceMultiplier / 4.0
            for (i in priceShares.indices) {
                priceShares[i] += priceShare
            }
        }
    }

    // Output
    //  67.850, 67.850, 51.750
    println("Teman 1 dan 2 yang tidak alergi ikan masing-masing membayar ${priceShares[1]} dan ${priceShares[2]}")
    println("Teman 3 yang alergi ikan membayar ${priceShares[3]}")
}