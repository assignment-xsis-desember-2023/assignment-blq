fun main() {
    // Input
    print("Input: ")
    val inputStr = readln()

    // Output
    val starCount = inputStr.length / 2
    for (i in inputStr.reversed()) {
        for (j in 1..starCount) print("*")
        print(i)
        for (j in 1..starCount) print("*")
        println()
    }
}