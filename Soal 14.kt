fun main() {
    // Input
    //  3 9 0 7 1 2 4
    print("Deret: ")
    val deretStr = readln().split(" ")
    val deretInt = Array(deretStr.size){0}
    for (i in 0..deretStr.lastIndex) {
        deretInt[i] = deretStr[i].toInt()
    }
    //  3
    //  1
    print("N = ")
    val rotation = readln().toInt()

    // Logic
    val rotatedDeret = Array(deretInt.size){0}
    for (i in 0..deretInt.lastIndex) {
        val newI = ((i - rotation) + deretInt.size) % deretInt.size
        rotatedDeret[newI] = deretInt[i]
    }

    // Output
    //  7 1 2 4 3 9 0
    //  9 0 7 1 2 4 3
    print(rotatedDeret.joinToString(" "))
}