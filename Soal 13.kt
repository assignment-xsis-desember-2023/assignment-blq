import java.lang.Double.max
import kotlin.math.abs
import kotlin.math.min

fun main() {
    // Constants
    val fullRotation = 360.0
    val minutePerRotation = 60
    val hourPerRotation = 12

    // Input
    //  3:00
    //  5:30
    //  2:20
    print("Input a time (format 'HH:mm'): ")
    val timeStr = readln().trim()

    // Logic
    val (hourRaw, minuteRaw) = timeStr.split(":").map { it.toInt() }
    val hour = hourRaw % 12
    val minute = minuteRaw % 60
    val minuteDegree = minute * fullRotation / minutePerRotation
    val hourDegree = (hour * fullRotation / hourPerRotation) + (minute * (fullRotation / hourPerRotation) / minutePerRotation)
    val possibleDifferences = arrayOf(
        abs(hourDegree - minuteDegree),
        fullRotation - max(hourDegree, minuteDegree) + min(hourDegree, minuteDegree)
    )
    val difference = possibleDifferences.min()

    // Output
    //  90.0
    //  15.0
    //  50.0
    print("Selisih antara jarum jam panjang dan pendek: $difference")
}