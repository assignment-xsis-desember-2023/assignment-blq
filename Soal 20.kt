fun main() {
    // Constants
    val numRound = 3

    // Input
    print("Jarak awal: ")
    val initialDistance = readln().trim().toInt()
    print("$numRound Gerakan A (G/K/B): ")
    val movesA = readln().uppercase().replace(Regex("[^GKB]"), "")
    if (movesA.length != numRound) {
        println("Invalid input")
        return
    }
    print("$numRound Gerakan B (G/K/B): ")
    val movesB = readln().uppercase().replace(Regex("[^GKB]"), "")
    if (movesB.length != numRound) {
        println("Invalid input")
        return
    }

    // Logic
    var winner = "Draw"
    var distance = initialDistance
    for (round in 1..numRound) {
        val roundWinner = rockPaperScissors(movesA[round-1], movesB[round-1])
        if (roundWinner != "Draw") {
            distance--
            if (distance == 0) {
                winner = roundWinner
            }
        }
    }

    // Output
    print("Hasil: $winner")
    if (winner != "Draw") print(" menang")
}

fun rockPaperScissors(moveA: Char, moveB: Char): String {
    return if (moveA == 'G' && moveB == 'K'
        || moveA == 'K' && moveB == 'B'
        || moveA == 'B' && moveB == 'G'
    )
        "A"
    else if (moveB == 'G' && moveA == 'K'
        || moveB == 'K' && moveA == 'B'
        || moveB == 'B' && moveA == 'G'
    )
        "B"
    else
        "Draw"
}