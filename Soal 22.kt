fun main() {
    // Input
    //  3	3	9	6	7	8	23
    print("Masukkan sederet angka (panjang masing-masing lilin): ")
    val deretStr = readln().trim().split(Regex("\\s"))
    val deretInt = Array(deretStr.size){0}
    deretStr.forEachIndexed { index, s -> deretInt[index] = s.toInt() }

    // Logic
    val meltingRates = Array(deretInt.size){0.0}
    var fib1 = 1
    var fib2 = 1
    var temp: Int
    for (i in meltingRates.indices) {
        meltingRates[i] = fib1.toDouble()
        temp = fib1
        fib1 = fib2
        fib2 = fib1 + temp
    }

    val meltingTimes = meltingRates.mapIndexed { index, meltingRate ->
        deretInt[index] / meltingRate
    }
    val firstToMelt = meltingTimes.indexOf(meltingTimes.min()) + 1

    // Output
    //  Lilin ke-6
    println("Lilin yang pertama meleleh sepenuhnya adalah lilin ke-$firstToMelt")
}