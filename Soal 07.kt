fun main() {
    // Input
    //  8 7 0 2 7 1 7 6 3 0 7 1 3 4 6 1 6 4 3
    print("Enter a string of numbers: ")
    val deretStr = readln().trim().split(" ")
    val deretInt = Array(deretStr.size){0}
    for (i in 0..deretStr.lastIndex) {
        deretInt[i] = deretStr[i].toInt()
    }

    // Logic
    // Mean
    val sum = deretInt.sum().toDouble()
    val mean = sum/deretInt.size

    // Median
    val sortedDeret = deretInt.sorted()
    val median = if (sortedDeret.size % 2 != 0) {
        sortedDeret[sortedDeret.size/2].toDouble()
    } else {
        (sortedDeret[sortedDeret.size/2 - 1] + sortedDeret[sortedDeret.size/2]) / 2.0
    }

    // Modus
    var modus: Int? = null
    var maxCount = 0
    var currentCount = 0
    var prevNum: Int? = null
    for (num in sortedDeret) {
        if (prevNum == null || prevNum == num) {
            currentCount++
        } else {
            currentCount = 1
        }

        if (currentCount > maxCount) {
            maxCount = currentCount
            modus = num
        }

        prevNum = num
    }

    // Output
    // Mean: 4
    // Median: 4
    // Modus: 7
    println("Mean: $mean")
    println("Median: $median")
    println("Modus: $modus")
}