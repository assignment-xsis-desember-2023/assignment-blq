fun main() {
    // Input
    //  03:40:44 PM
    print("Masukkan jam dalam format 'HH:mm:ss AM' atau 'HH:mm:ss PM': ")
    val input12Hr = readln().trim()

    // Logic
    val inputHour = input12Hr.substring(0..1)
    val minuteSecondStr = input12Hr.substring(2..7)
    val period = input12Hr.substring(8).trim()
    val outputHour = if (period == "AM") {
        inputHour.toInt() % 12
    } else {
        inputHour.toInt() % 12 + 12
    }
    val outputHourStr = String.format("%02d", outputHour)

    // Output
    //  15:40:44
    print("$outputHourStr$minuteSecondStr")
}