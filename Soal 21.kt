fun main() {
    // Input
    print("Panjang lintasan: ")
    val length = readln().trim().toInt()
    if (length <= 0) {
        println("Input invalid (panjang lintasan harus positif)")
        return
    }

    print("Deret posisi lubang: ")
    val inputStr = readln().trim()
    val listLubang = if (inputStr.isEmpty())
        emptyList()
    else
        inputStr.split(" ").map { it.toInt() }
    if (length in listLubang) {
        println("Input invalid (lubang tidak boleh ada di ujung lintasan)")
        return
    }

    // Logic
    var stamina = 0
    var position = 0
    val moveRecord = arrayListOf<String>()
    val possibleMoves = Array(length){arrayListOf("J", "W")}
    var backtrackedMove: String? = null
    while (position != length) {
        if (!backtrackedMove.isNullOrBlank()) {
            possibleMoves[position].remove(backtrackedMove)
        }
        if (stamina < 2) {
            possibleMoves[position].remove("J")
        }
        if (position + 3 in listLubang || position + 3 > length) {
            possibleMoves[position].remove("J")
        }
        if (position + 1 in listLubang) {
            possibleMoves[position].remove("W")
        }

        if (possibleMoves[position].isEmpty()) {
            backtrackedMove = moveRecord.removeLastOrNull()
            when (backtrackedMove) {
                "W" -> { stamina -= 1; position -= 1 }
                "J" -> { stamina += 2; position -= 3 }
                null -> break
            }
        } else {
            backtrackedMove = null
            val move = possibleMoves[position].first()
            moveRecord.add(move)
            when (move) {
                "W" -> {
                    stamina += 1
                    position += 1
                    if (position != length) possibleMoves[position] = arrayListOf("J", "W")
                }
                "J" -> {
                    stamina -= 2
                    position += 3
                    if (position != length) possibleMoves[position] = arrayListOf("J", "W")
                }
            }
        }
    }

    // Output
    if (moveRecord.isNotEmpty()) {
        println("Gerakan minimal yang mungkin adalah ${moveRecord.joinToString(" ")}")
    } else {
        println("FAILED")
    }
}