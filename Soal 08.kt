fun main() {
    // Input
    //  1 2 4 7 8 6 9
    print("Enter a string of numbers: ")
    val deretStr = readln().trim().split(" ")
    val deretInt = Array(deretStr.size){0}
    for (i in 0..deretStr.lastIndex) {
        deretInt[i] = deretStr[i].toInt()
    }

    // Logic
    val sortedDeret = deretInt.sorted()
    val minFourElementsSum = sortedDeret.subList(0, 4).sum()
    val maxFourElementsSum = sortedDeret.subList(sortedDeret.size-4, sortedDeret.size).sum()

    // Output
    //  13; 30
    println("Nilai minimal dari penjumlahan 4 komponen: $minFourElementsSum")
    println("Nilai maksimal dari penjumlahan 4 komponen: $maxFourElementsSum")
}