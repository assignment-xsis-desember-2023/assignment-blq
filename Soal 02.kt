import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.util.*

fun main() {
    // Constants
    val booksDayLimit = arrayOf(14, 3, 7, 7)
    val fineRatePerDay = 100

    // Input
    //  a. 28 Februari 2016 – 7 Maret 2016
    //  b. 29 April 2018 – 30 Mei 2018
    print("Tanggal mulai meminjam: ")
    val startDateStr = readln()
    print("Tanggal selesai meminjam: ")
    val endDateStr = readln()

    // Logic
    val dateFormatter = DateTimeFormatter.ofPattern("d MMMM yyyy", Locale("id"))
    val startDate = LocalDate.from(dateFormatter.parse(startDateStr))
    val endDate = LocalDate.from(dateFormatter.parse(endDateStr))

    val numOfDays: Int
    if (startDate > endDate) {
        println("Invalid date range")
        return
    }
    else {
        var i = 0L
        while (startDate.plusDays(i) <= endDate) {
            i++
        }
        numOfDays = i.toInt()
    }

    val lateDays = booksDayLimit.map { numOfDays - it }
    val fineArr = lateDays.map { if (it > 0) it * fineRatePerDay else 0 }

    // Output
    //  a. 1000 (0, 600, 200, 200)
    //  b. 9700 (1800, 2900, 2500, 2500)
    println("Denda Buku A: ${fineArr[0]}")
    println("Denda Buku B: ${fineArr[1]}")
    println("Denda Buku C: ${fineArr[2]}")
    println("Denda Buku D: ${fineArr[3]}")
    println("Denda Total: ${fineArr.sum()}")
}