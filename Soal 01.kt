fun main() {
    // Input
    //  Uang: 1000
    //   500 600 700 800
    //   200 400 350
    //   400 350 200 300
    //   100 50 150
    print("Uang: ")
    val money = readln().trim().toInt()
    print("Banyak jenis barang yang tersedia: ")
    val availableItemCount = readln().trim().toInt()
    val priceTable = Array<List<Int>>(availableItemCount){ listOf() }
    for (i in 1..availableItemCount) {
        print("Masukkan sederet harga untuk barang jenis ke-$i: ")
        val itemPrices = readln().trim().split(Regex("\\s")).map { it.toInt() }
        priceTable[i-1] = itemPrices
    }

    // Logic
    var maxSpentMoney = money
    var choices = arrayListOf<Int>()
    while (maxSpentMoney > 0 && choices.isEmpty()) {
        choices = findPotentialItemChoices(priceTable, maxSpentMoney).maxByOrNull { it.count { it != 0 } } ?: arrayListOf()
        if (choices.isEmpty()) {
            maxSpentMoney--
        }
    }

    // Output
    println("Banyak maksimal uang yang bisa dikeluarkan: $maxSpentMoney")
    println("Item yang dibeli: ")
    for ((index, item) in choices.withIndex()) {
        if (item != 0) {
            println("- Item ke-${index+1} seharga $item")
        }
    }
}

fun findPotentialItemChoices(table: Array<List<Int>>, money: Int, item: Int = 0): ArrayList<ArrayList<Int>> {
    var remainingMoney = money
    val potentialChoices = arrayListOf<ArrayList<Int>>()
    for (i in -1..table[item].lastIndex) {
        val price = if (i != -1) table[item][i] else 0
        if (remainingMoney - price < 0) {
            continue
        } else {
            remainingMoney -= price
            if (item != table.lastIndex) {
                val potentialNextChoices = findPotentialItemChoices(table, remainingMoney, item+1)
                for (nextChoices in potentialNextChoices) {
                    if (remainingMoney - nextChoices.sum() == 0) {
                        val chosenMoney = arrayListOf<Int>()
                        chosenMoney.add(price)
                        chosenMoney.addAll(nextChoices)
                        potentialChoices.add(chosenMoney)
                    }
                }
            } else {
                if (remainingMoney == 0) {
                    val chosenMoney = arrayListOf<Int>()
                    chosenMoney.add(price)
                    potentialChoices.add(chosenMoney)
                }
            }
            remainingMoney += price
        }
    }
    return potentialChoices
}