fun main() {
    // Input
    //  N N T N N N T T T T T N T T T N T N
    print("Masukkan deretan karakter 'N' (naik) dan 'T' (turun): ")
    val input = readln().uppercase().replace(Regex("[^NT]"), "")

    // Logic
    var height = 0
    var gunungCount = 0
    var lembahCount = 0
    for (action in input) {
        when (action) {
            'N' -> height++
            'T' -> height--
        }

        if (height == 0) {
            when (action) {
                'N' -> lembahCount++
                'T' -> gunungCount++
            }
        }
    }

    // Output
    println("Hattori sudah melewati $gunungCount gunung dan $lembahCount lembah")
}