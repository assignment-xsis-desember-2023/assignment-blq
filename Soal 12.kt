fun main() {
    // Input
    //  1 2 1 3 4 7 1 1 5 6 1 8
    print("Input a stream of numbers: ")
    val deretStr = readln().trim().split(" ")
    val deretInt = Array(deretStr.size){0}
    for (i in deretStr.indices) {
        deretInt[i] = deretStr[i].toInt()
    }

    // Logic
    val newDeret = mutableListOf<Int>()
    val doneIndices = mutableListOf<Int>()
    for (i in deretInt.indices) {
        var minValue = Int.MAX_VALUE
        var minIndex = -1
        for (j in deretInt.indices) {
            if (j in doneIndices) continue

            if (deretInt[j] < minValue) {
                minValue = deretInt[j]
                minIndex = j
            }
        }
        newDeret.add(minValue)
        doneIndices.add(minIndex)
    }

    // Output
    //  1 1 1 1 1 2 3 4 5 6 7 8
    print("Sorted: ${newDeret.joinToString(" ")}")
}