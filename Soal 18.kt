fun main() {
    // Data
    val eatingData = mapOf(9 to 30, 13 to 20, 15 to 50, 17 to 80)
    val exerciseHour = 18

    // Logic
    val exerciseLength = eatingData.map { (eatingHour, calories) ->
        0.1 * calories * (exerciseHour - eatingHour)
    }.sum()
    val drink = exerciseLength.toInt()/30 * 100 + 500

    // Output
    //  700
    println("Donna minum $drink cc air")
}