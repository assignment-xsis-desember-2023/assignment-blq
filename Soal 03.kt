import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

fun main() {
    // Input
    //  27 Januari 2019 | 05:00:01 – 27 Januari 2019 | 17:45:03
    //  27 Januari 2019 | 07:03:59 – 27 Januari 2019 | 15:30:06
    //  27 Januari 2019 | 07:05:00 – 28 Januari 2019 | 00:20:21
    //  27 Januari 2019 | 11:14:23 – 27 Januari 2019 | 13:20:00
    print("Tanggal dan waktu masuk (format 'Tanggal | Jam:Menit:Detik'): ")
    val startDateTimeStr = readln().trim()
    print("Tanggal dan waktu keluar (format 'Tanggal | Jam:Menit:Detik'): ")
    val endDateTimeStr = readln().trim()

    // Logic
    val dateTimeFormatter = DateTimeFormatter.ofPattern("d MMMM yyyy | HH:mm:ss")
    val startDateTime = LocalDateTime.from(dateTimeFormatter.parse(startDateTimeStr))
    val endDateTime = LocalDateTime.from(dateTimeFormatter.parse(endDateTimeStr))
    var parkingHour = 0
    while (startDateTime.plusHours(parkingHour.toLong() + 1) <= endDateTime) {
        parkingHour++
    }
    val parkingFee = calculateParkingFee(parkingHour)

    // Output
    //  8000
    //  8000
    //  8000
    //  2000
    print("Tarif: $parkingFee")
}

// Function requested by assignment
fun calculateParkingFee(parkingHour: Int): Int {
    val hourlyRate = 1000
    val flatHourlyFee = 8000
    val dailyRate = 15000

    val hourlyFee = if (parkingHour % 24 < 8) {
        hourlyRate * (parkingHour % 24)
    } else {
        flatHourlyFee
    }

    return dailyRate * (parkingHour/24) + hourlyFee
}