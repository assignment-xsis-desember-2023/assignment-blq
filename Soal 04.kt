fun main() {
    // Input
    print("n = ")
    val n = readln().toInt()

    // Output
    printFirstPrimeNumbers(n)
}

// Function requested by the assignment
fun printFirstPrimeNumbers(n: Int) {
    var i = 2
    var count = 0
    while (count < n) {
        if (i.isPrime()) {
            print(i)
            count++
            if (count != n) print(" ")
        }
        i++
    }
}

fun Int.isPrime(): Boolean {
    for (i in 2 until this) {
        if (this % i == 0) return false
    }
    return true
}