fun main() {
    // Input
    //  “Sphinx of black quartz, judge my vow”
    //  “Brawny gods just flocked up to quiz and vex him”
    //  “Check back tomorrow; I will see if the book has arrived.”
    print("Input some text: ")
    val text = readln()

    // Logic
    val preprocessedText = text.lowercase()
    var isPangram = true
    for (char in 'a'..'z') {
        if (char !in preprocessedText) {
            isPangram = false
        }
    }

    // Output
    //  true
    //  true
    //  false
    when (isPangram) {
        true -> print("The text is a pangram")
        false -> print("The text is not a pangram")
    }
}